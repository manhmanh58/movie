import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getMovies } from "api/axiosCient";

const moviesSlice = createSlice({
  name: "movieList",
  initialState: { status: "idle", movies: [], loading: false },
  reducers: {
    reset: (state, action) => {
      state.movies = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchMovies.pending, (state, action) => {
        state.status = "loading";
        state.loading = true;
      })
      .addCase(fetchMovies.fulfilled, (state, action) => {
        state.movies.push(...action.payload.results);
        state.status = "idle";
        state.loading = false;
      })
      .addCase(fetchMovies.rejected, (state, action) => {
        state.status = "failed";
        state.loading = false;
      });
  },
});

export const fetchMovies = createAsyncThunk(
  "movies/fetchMovies",
  async (page) => {
    const res = await getMovies(page);
    return res;
  }
);

export default moviesSlice;
