import { configureStore } from "@reduxjs/toolkit";
import moviesSlice from "./movies/moviesSlice";
import detailSlice from "./detail/detailSlice";

const store = configureStore({
  reducer: {
    movieList: moviesSlice.reducer,
    detail: detailSlice.reducer,
  },
});

export default store;
