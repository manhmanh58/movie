import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  getMovieDetails,
  getTrailer,
  getCredit,
  getReview,
  getRecommendation,
  getKeyword,
  getPoster,
} from "../../api/axiosCient";

const detailSlice = createSlice({
  name: "movieDetail",
  initialState: {
    detail: [],
    trailer: [],
    profile: [],
    review: [],
    recommendations: [],
    keyword: [],
    images: [],
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchDetail.fulfilled, (state, action) => {
        state.detail = action.payload;
      })
      .addCase(fetchTrailer.fulfilled, (state, action) => {
        state.trailer = action.payload.results;
      })
      .addCase(fetchProfile.fulfilled, (state, action) => {
        state.profile = action.payload;
      })
      .addCase(fetchReview.fulfilled, (state, action) => {
        state.review = action.payload.results;
      })
      .addCase(fetchRecommendation.fulfilled, (state, action) => {
        state.recommendations = action.payload.results;
      })
      .addCase(fetchKeyword.fulfilled, (state, action) => {
        state.keyword = action.payload.keywords;
      })
      .addCase(fetchPoster.fulfilled, (state, action) => {
        state.images = action.payload;
      });
  },
});

export const fetchDetail = createAsyncThunk(
  "detail/fetchDetail",
  async (id) => {
    const res = await getMovieDetails(id);
    return res;
  }
);

export const fetchTrailer = createAsyncThunk(
  "detail/fetchTrailer",
  async (id) => {
    const res = await getTrailer(id);
    return res;
  }
);

export const fetchProfile = createAsyncThunk(
  "detail/fetchProfile",
  async (id) => {
    const res = await getCredit(id);
    return res;
  }
);

export const fetchReview = createAsyncThunk(
  "detail/fetchReview",
  async (id) => {
    const res = await getReview(id);
    return res;
  }
);

export const fetchRecommendation = createAsyncThunk(
  "detail/Recommendation",
  async (id) => {
    const res = await getRecommendation(id);
    return res;
  }
);

export const fetchKeyword = createAsyncThunk("detail/Keyword", async (id) => {
  const res = await getKeyword(id);
  return res;
});

export const fetchPoster = createAsyncThunk("detail/Poster", async (id) => {
  const res = await getPoster(id);
  return res;
});

export default detailSlice;
