import axios from "axios";
import { API_KEY, API_URL } from "../constants/common";
export const axiosClient = axios.create({
  baseURL: API_URL,
  timeout: 1000,
});

export const getMovies = async (pageParam = 1) => {
  const response = await axiosClient.get(
    `popular?api_key=${API_KEY}&language=en-US&page=${pageParam}`
  );
  return response.data;
};

export const getMovieDetails = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getTrailer = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}/videos?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getCredit = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}/credits?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getReview = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}/reviews?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getRecommendation = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}/recommendations?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getKeyword = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}/keywords?api_key=${API_KEY}&language=en-US`
  );
  return response.data;
};

export const getPoster = async (movieId) => {
  const response = await axiosClient.get(
    `${movieId}/images?api_key=${API_KEY}`
  );
  return response.data;
};
