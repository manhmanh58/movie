import React from "react";
import styles from "./style.module.scss";

const StyleBlur = ({ hideFade }) => {
  return (
    <div
      className={styles.blur}
      style={{ ...(hideFade === "1" ? { opacity: 1 } : { opacity: 0 }) }}
    ></div>
  );
};

export default StyleBlur;
