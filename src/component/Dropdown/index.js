import React, { useState } from "react";
import styles from "./style.module.scss";

const Dropdown = ({ children, labels }) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div
      onMouseEnter={() => setIsOpen(true)}
      onMouseLeave={() => setIsOpen(false)}
      style={{ display: "flex" }}
    >
      {children}
      {isOpen && (
        <div className={styles[`dropdown`]}>
          {labels.map((item, index) => {
            return (
              <div className={styles[`dropdown__item`]} key={index}>
                <a href="/">{item.label}</a>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Dropdown;
