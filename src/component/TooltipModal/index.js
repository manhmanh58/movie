import React, { useState } from "react";
import styles from "./style.module.scss";

const Tooltip = ({ children, message }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <li
      className={styles[`tooltip`]}
      onMouseEnter={() => setIsOpen(true)}
      onMouseLeave={() => setIsOpen(false)}
    >
      <div className={styles[`tooltip-box`]}>
        {children}
        {isOpen && (
          <div className={styles[`tooltip-box__popup`]}>{message}</div>
        )}
      </div>
    </li>
  );
};

export default Tooltip;
