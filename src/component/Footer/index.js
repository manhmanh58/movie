import React from "react";
import styles from "./style.module.scss";
import footer from "assets/images/Logofooter.svg";
import { Basics, INVOLVED, COMMUNITY, LEGAL } from "constants/footerMap";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <nav>
        <div className={styles[`join`]}>
          <img
            src={footer}
            alt="The Movie Database (TMDB)"
            width="130"
            height="94"
          />
          <a className={styles[`join__logo`]} href="/">
            Join the Community
          </a>
        </div>
        <div>
          <h3>The Basics</h3>
          <ul>
            {Basics.map((basic, index) => (
              <li key={index}>
                <a href="/">{basic.label}</a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Get Involved</h3>
          <ul>
            {INVOLVED.map((involve, index) => (
              <li key={index}>
                <a href="/">{involve.label}</a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Community</h3>
          <ul>
            {COMMUNITY.map((Community, index) => (
              <li key={index}>
                <a href="/">{Community.label}</a>
              </li>
            ))}
          </ul>
        </div>
        <div>
          <h3>Legal</h3>
          <ul>
            {LEGAL.map((legal, index) => (
              <li key={index}>
                <a href="/">{legal.label}</a>
              </li>
            ))}
          </ul>
        </div>
      </nav>
    </footer>
  );
};

export default Footer;
