const SORT_RESULTS = [
  'Popularity Descending',
  'Popularity Ascending',
  'Rating Descending',
  'Rating Ascending',
  'Release Date Descending',
  'Release Date Ascending',
  'Title (A-Z)',
  'Title (Z-A)',
];
const AVAILABILITIES = ['Stream', 'Free', 'Ads', 'Rent', 'Buy'];
const RELEASE = [
  'Search all countries?',
  'Premiere',
  'Theatrical (limited)',
  'Theatrical',
  'Digital',
  'Physical',
  'TV',
];
const GENRES = [
  'Action',
  'Adventure',
  'Animation',
  'Comedy',
  'Crime  ',
  'Documentary',
  'Drama',
  'Family',
  'Fantasy',
  'History',
  'Horror',
  'Music',
  'Mystery',
  'Romance',
  'Science Fiction',
  'TV Movie',
  'Thriller',
  'War',
  'Western',
];
export { SORT_RESULTS, AVAILABILITIES, RELEASE, GENRES };
