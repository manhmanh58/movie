export const MOVIES = [
  {
    label: "Popular",
  },
  {
    label: "Now Playing",
  },
  {
    label: "Upcoming",
  },
  {
    label: "Top Rated",
  },
];
export const TV_SHOWS = [
  {
    label: "Popular",
  },
  {
    label: "Airing Today",
  },
  {
    label: "On TV",
  },
  {
    label: "Top Rated",
  },
];
export const PEOPLE = [
  {
    label: "Popular People",
  },
];
export const MORE = [
  {
    label: "Discussions",
  },
  {
    label: "Leaderboard",
  },
  {
    label: "Support",
  },
  {
    label: "API",
  },
];
