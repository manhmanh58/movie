import MovieList from "pages/MovieLists";
import MovieDetail from "pages/MovieDetail";
import { Routes, Route } from "react-router-dom";
function Router() {
  return (
    <Routes>
      <Route path="/" element={<MovieList />} />
      <Route path="/movie/:id" element={<MovieDetail />} />
    </Routes>
  );
}

export default Router;
