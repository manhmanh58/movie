import React, { useState, useEffect } from "react";
import styles from "./style.module.scss";
import Video from "component/VideoModal";
import { useSelector, useDispatch } from "react-redux";
import { fetchTrailer } from "redux/detail/detailSlice";
import { useParams } from "react-router";
import { BACKDROP_URL } from "constants/common";
import Poster from "./Poster";
import Description from "./PosterDescription";

const HeaderBackground = () => {
  const { detail, trailer } = useSelector((state) => state.detail);
  const { id } = useParams();
  const trailers = trailer?.find((c) => c.type === "Trailer");
  const [isModalPosterOpen, setModalPosterOpen] = useState(false);
  const [isTrailerPlay, setTrailerPlay] = useState(false);
  const percentage = Math.round(detail.vote_average * 10);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchTrailer(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div
      className={styles[`header-lagre`]}
      style={{
        backgroundImage: `url(${BACKDROP_URL}${detail.backdrop_path})`,
      }}
    >
      <div className={styles[`header-lagre__custom-bg`]}>
        <Poster
          poster_path={detail.poster_path}
          isModalPosterOpen={isModalPosterOpen}
          setModalPosterOpen={setModalPosterOpen}
        />
        <Description
          title={detail.title}
          release_date={detail.release_date}
          genres={detail.genres}
          runtime={detail.runtime}
          tagline={detail.tagline}
          overview={detail.overview}
          percentage={percentage}
          setTrailerPlay={setTrailerPlay}
        />
      </div>
      {isTrailerPlay && (
        <Video
          trailer={trailers?.key}
          isTrailerPlay={isTrailerPlay}
          setTrailerPlay={setTrailerPlay}
        />
      )}
    </div>
  );
};

export default HeaderBackground;
