import React from "react";
import styles from "./style.module.scss";
import { CircularProgressbar } from "react-circular-progressbar";
import { AiOutlinePercentage } from "react-icons/ai";
import dateFormat from "dateformat";
import Tooltip from "../../../../../component/TooltipModal";
import { chain } from "lodash";
import { useSelector } from "react-redux";

const Description = ({
  title,
  release_date,
  genres,
  runtime,
  tagline,
  overview,
  setTrailerPlay,
  percentage,
}) => {
  const crewApi = useSelector((state) => state.detail.profile.crew);
  const crew = chain(crewApi)
    .filter((obj) => {
      const jobs = ["Characters", "Screenplay", "Director", "Story", "Writer"];
      return jobs.includes(obj.job);
    })
    .orderBy(["popularity"], ["desc"])
    .groupBy("name")
    .transform(function (result, val, key) {
      result.push({ [key]: val.map((i) => i.job).join(", ") });
    }, [])
    .value();

  const handleClick = () => {
    setTrailerPlay(true);
  };
  return (
    <div className={styles[`header-poster`]}>
      <div className={styles[`header-poster__title`]}>
        <h2>
          <a href="/">{title}</a>
          <span className={styles[`header-poster__release-date`]}>
            ({dateFormat(release_date, "yyyy")})
          </span>
        </h2>
        <div className={styles[`header-poster__facts`]}>
          <span className={styles[`header-poster__certification`]}>PG-13</span>
          <span className={styles[`header-poster__release`]}>
            {dateFormat(release_date, "mm/dd/yyyy")} (US)
          </span>
          <span className={styles[`header-poster__genres`]}>
            {genres?.map((genre, index) => (
              <a key={index} href="/">
                {genre.name}
              </a>
            ))}
          </span>
          <span className={styles[`header-poster__runtime`]}>{`${Math.floor(
            runtime / 60
          )}h ${runtime % 60}m`}</span>
        </div>
      </div>
      <ul className={styles.action}>
        <li className={styles.chart}>
          <div className={styles.details}>
            <div className={styles.outer_ring}>
              <div className={styles.user_score}>
                <CircularProgressbar
                  value={percentage}
                  text={`${percentage}`}
                  styles={{
                    // Customize the path
                    path: {
                      stroke:
                        percentage >= 70
                          ? `rgba(33, 208, 122, ${percentage / 100})`
                          : `rgba(210,213,49, ${percentage / 100})`,

                      strokeLinecap: "round",
                    },
                    // Customize the circle behind the path
                    trail: {
                      stroke: percentage >= 70 ? "#204529" : "#423d0f",
                      strokeLinecap: "round",
                    },
                    text: {
                      fill: "#f2f3f4",
                      fontSize: "2.3rem",
                      fontWeight: "bold",
                    },
                  }}
                />
                <div className={styles.percent}>
                  <AiOutlinePercentage size={8} style={{ fill: "white" }} />
                </div>
              </div>
            </div>
          </div>
          <div className={styles.text}>
            User
            <br />
            Score
          </div>
        </li>
        <Tooltip message="Login to create and edit custom lists">
          <span className={styles.tooltip__list}></span>
        </Tooltip>
        <Tooltip message="Login to create and edit custom lists">
          <span className={styles.tooltip__heart}></span>
        </Tooltip>
        <Tooltip message="Login to create and edit custom lists">
          <span className={styles.tooltip__bookmark}></span>
        </Tooltip>
        <Tooltip message="Login to rate this movie">
          <span className={styles.tooltip__rating}></span>
        </Tooltip>
        <li className={styles.video}>
          <p className={styles.video__content} onClick={handleClick}>
            <span className={styles.video__play}> </span>Play Trailer
          </p>
        </li>
      </ul>
      <div className={styles[`header-info`]}>
        <h3 className={styles[`header-info__tagline`]}>{tagline}</h3>
        <h3>Overview</h3>
        <div className={styles[`header-info__overview`]}>
          <p>{overview}</p>
        </div>
        <ol className={styles[`header-info__people`]}>
          {crew.slice(0, 5).map((item, index) => {
            const key = Object.keys(item)[0];
            return (
              <li className={styles[`header-info__profile`]} key={index}>
                <p>
                  <a href="/">{key}</a>
                </p>
                <p className={styles[`header-info__character`]}>{item[key]}</p>
              </li>
            );
          })}
        </ol>
      </div>
    </div>
  );
};

export default Description;
