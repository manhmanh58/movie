import React from "react";
import styles from "./style.module.scss";

const LeaderBoard = () => {
  return (
    <div className={styles[`leader-board`]}>
      <h4>Top Contributors</h4>
      <div className={styles[`leader-board__edit`]}>
        <div className={styles[`leader-board__avatar`]}>
          <a href="/">
            <img
              alt="avatar"
              className={styles[`leader-board__image`]}
              src=""
            />
          </a>
        </div>
        <div className={styles[`leader-board__info`]}></div>
      </div>
      <div className={styles[`leader-board__edit`]}>
        <a href="/">
          <img alt="avatar" className={styles[`leader-board__image`]} src="" />
        </a>
        <div className={styles[`leader-board__avatar`]}></div>
        <div className={styles[`leader-board__info`]}></div>
      </div>
      <div className={styles[`leader-board__edit`]}>
        <a href="/">
          <img alt="avatar" className={styles[`leader-board__image`]} src="" />
        </a>
        <div className={styles[`leader-board__avatar`]}></div>
        <div className={styles[`leader-board__info`]}></div>
      </div>
      <div className={styles[`leader-board__edit`]}>
        <a href="/">
          <img alt="avatar" className={styles[`leader-board__image`]} src="" />
        </a>
        <div className={styles[`leader-board__avatar`]}></div>
        <div className={styles[`leader-board__info`]}></div>
      </div>
    </div>
  );
};

export default LeaderBoard;
