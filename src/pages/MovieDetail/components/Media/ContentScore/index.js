import React from "react";
import styles from "./style.module.scss";

const ContentScore = () => {
  return (
    <div className={styles.content}>
      <h4 className={styles.content__title}>Content Score</h4>
      <div className={styles[`content__progress`]}>
        <div className={styles[`content__true`]}>
          <p className={styles[`content__score`]}>88</p>
        </div>
      </div>
      <p className={styles[`content__score--status`]}>Almost there...</p>
    </div>
  );
};

export default ContentScore;
