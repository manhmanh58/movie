import React from "react";
import styles from "./style.module.scss";
const Discussions = () => {
  return (
    <div className={styles.discussions}>
      <div className={styles.discussions__post}>
        <div className={styles[`discussions__post--info`]}>
          <div className={styles[`discussions__post--avatars`]}>
            <span
              className={styles[`discussions__post--avatar`]}
              style={{ backgroundColor: "#EA148C" }}
            >
              <a href="/">
                <span className={styles[`discussions__post--pink`]}>J</span>
              </a>
            </span>
          </div>
          <div className={styles[`discussions__post--link`]}>
            <a className={styles.discussions__topic} href="/">
              Serious question? Could you wrap rope around the pole and your
              waist, like telephone pole climber? I know it is slick...
            </a>
          </div>
        </div>
        <div className={styles.discussions__status}>
          <p className={styles[`discussions__status--open`]}>Open</p>
        </div>
        <div className={styles.discussions__status}>
          <p>6</p>
        </div>
        <div className={styles.discussions__status}>
          <p>
            Sep 20, 2022 at 10:39 AM
            <br />
            by
            <span className={styles[`discussions__username`]}>
              <a href="/"> JonBron1864</a>
            </span>
          </p>
        </div>
      </div>
      <div className={styles.discussions__post}>
        <div className={styles[`discussions__post--info`]}>
          <div className={styles[`discussions__post--avatars`]}>
            <span
              className={styles[`discussions__post--avatar`]}
              style={{ backgroundColor: "#959595" }}
            >
              <a href="/">
                <span className={styles[`discussions__post--pink`]}>A</span>
              </a>
            </span>
          </div>
          <div className={styles[`discussions__post--link`]}>
            <a className={styles.discussions__topic} href="/">
              No Parachutes?
            </a>
          </div>
        </div>
        <div className={styles.discussions__status}>
          <p className={styles[`discussions__status--open`]}>Open</p>
        </div>
        <div className={styles.discussions__status}>
          <p>6</p>
        </div>
        <div className={styles.discussions__status}>
          <p>
            Sep 19, 2022 at 10:52 AM
            <br />
            by
            <span className={styles[`discussions__username`]}>
              <a href="/"> NJskydiver</a>
            </span>
          </p>
        </div>
      </div>
      <div className={styles.discussions__post}>
        <div className={styles[`discussions__post--info`]}>
          <div className={styles[`discussions__post--avatars`]}>
            <span
              className={styles[`discussions__post--avatar`]}
              style={{ backgroundColor: "#D29001" }}
            >
              <a href="/">
                <span className={styles[`discussions__post--pink`]}>D</span>
              </a>
            </span>
          </div>
          <div className={styles[`discussions__post--link`]}>
            <a className={styles.discussions__topic} href="/">
              Serious question? Could you wrap rope around the pole and your
              waist, like telephone pole climber? I know it is slick...
            </a>
          </div>
        </div>
        <div className={styles.discussions__status}>
          <p className={styles[`discussions__status--open`]}>Open</p>
        </div>
        <div className={styles.discussions__status}>
          <p>6</p>
        </div>
        <div className={styles.discussions__status}>
          <p>
            Sep 20, 2022 at 10:39 AM
            <br />
            by
            <span className={styles[`discussions__username`]}>
              <a href="/"> JonBron1864</a>
            </span>
          </p>
        </div>
      </div>
      <p className={styles[`media-panel__button`]}>
        <a href="/">Go to Discussions</a>
      </p>
    </div>
  );
};

export default Discussions;
