import React from "react";
import styles from "./style.module.scss";
import Image from "assets/images/Trend.png";

const Trend = () => {
  return (
    <div className={styles.trend}>
      <h4 className={styles.trend__title}>Popularity Trend</h4>
      <img alt="Trend" src={Image}></img>
    </div>
  );
};

export default Trend;
