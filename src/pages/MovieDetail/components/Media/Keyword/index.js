import React from "react";
import styles from "./style.module.scss";
import { useSelector } from "react-redux";

const KeyWord = () => {
  const keyWordApi = useSelector((state) => state.detail.keyword);
  return (
    <div className={styles[`key-word`]}>
      <h4>
        <bdi>Keywords</bdi>
      </h4>
      <ul className={styles[`key-word__lists`]}>
        {keyWordApi?.map((key, index) => (
          <li key={index} className={styles[`key-word__list`]}>
            <a href="/">{key.name}</a>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default KeyWord;
