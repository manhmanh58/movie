import React, { useState } from "react";
import styles from "./style.module.scss";
import Video from "component/VideoModal";

const VideoCard = ({ trailer }) => {
  const [isTrailerPlay, setTrailerPlay] = useState(false);
  return (
    <>
      <div
        className={styles[`videos__video`]}
        style={{
          backgroundImage: `url(${`https://i.ytimg.com/vi/${trailer.key}/hqdefault.jpg`})`,
        }}
        onClick={() => setTrailerPlay(true)}
      >
        <div className={styles[`videos__trailer`]}>
          <span className={styles[`videos__play-trailer`]}></span>
        </div>
      </div>
      {isTrailerPlay && (
        <Video
          trailer={trailer?.key}
          isTrailerPlay={isTrailerPlay}
          setTrailerPlay={setTrailerPlay}
        />
      )}
    </>
  );
};

export default VideoCard;
