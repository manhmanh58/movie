import React from "react";
import styles from "./style.module.scss";
const Posters = ({ posters }) => {
  return (
    <>
      {posters.slice(0, 5).map((poster, index) => (
        <div className={styles[`posters__poster`]} key={index}>
          <img
            alt="poster"
            src={`https://www.themoviedb.org/t/p/w220_and_h330_face${poster.file_path}`}
          />
        </div>
      ))}
      <div className={styles[`view-more`]}>
        <p>
          View More
          <span className={styles[`view-more__array-icon`]}></span>
        </p>
      </div>
    </>
  );
};

export default Posters;
