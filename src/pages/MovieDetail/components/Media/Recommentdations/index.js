import React, { useState } from "react";
import styles from "./style.module.scss";
import { useSelector } from "react-redux";
import { RECOMMENDATION_URL } from "constants/common";
import dateFormat from "dateformat";
import StyleBlur from "component/StyleBlur";

const Recommendations = () => {
  const recommendationApi = useSelector(
    (state) => state.detail.recommendations
  );
  const [scrollPosition, setScrollPosition] = useState(0);
  const onScroll = (e) => {
    const position = e.target.scrollLeft;
    setScrollPosition(position);
  };

  return (
    <div className={styles[`recommendations`]}>
      <h3 className={styles[`recommendations__title`]}>Recommendations</h3>
      <div className={styles[`recommendations__scrolled`]} onScroll={onScroll}>
        {recommendationApi.map((recomment, index) => (
          <div key={index} className={styles[`recommendations__item`]}>
            {recomment.backdrop_path !== null ? (
              <img
                loading="lazy"
                alt="item"
                src={`${RECOMMENDATION_URL}${recomment.backdrop_path}`}
              />
            ) : (
              <div className={styles[`recommendations__picture`]}> </div>
            )}

            <p className={styles[`recommendations__movie`]}>
              <a href="/" className={styles[`recommendations__title`]}>
                {recomment.title.length > 28
                  ? recomment.title.substring(0, 28) + "..."
                  : recomment.title}
              </a>
              <span className={styles[`recommendations__vote-average`]}>
                {Math.round(recomment.vote_average)}%
              </span>
            </p>
            <div className={styles[`recommendations__meta`]}>
              <span className={styles[`recommendations__realease-date`]}>
                <span className={styles[`recommendations__calender`]}></span>
                {dateFormat(recomment.release_date, "paddedShortDate")}
              </span>
            </div>
          </div>
        ))}
        <StyleBlur hideFade={scrollPosition > 100 ? null : "1"} />
      </div>
    </div>
  );
};

export default Recommendations;
