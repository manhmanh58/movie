import React, { useEffect } from "react";
import styles from "./style.module.scss";
import Header from "component/Header";
import ShortcutBar from "./components/ShortcutBar/index";
import Footer from "component/Footer";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchDetail,
  fetchProfile,
  fetchReview,
  fetchRecommendation,
  fetchKeyword,
  fetchPoster,
} from "redux/detail/detailSlice";
import { useParams } from "react-router";
import Media from "./components/Media";

import HeaderBackground from "./components/HeaderBackground";

const MovieDetail = () => {
  const detailApi = useSelector((state) => state.detail.detail);
  const dispatch = useDispatch();
  const { id } = useParams();

  useEffect(() => {
    dispatch(fetchReview(id));
    dispatch(fetchDetail(id));
    dispatch(fetchProfile(id));
    dispatch(fetchRecommendation(id));
    dispatch(fetchKeyword(id));
    dispatch(fetchPoster(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div>
      <Header />
      <div className={styles.main}>
        <ShortcutBar />
        <HeaderBackground id={{ id }} {...detailApi} />
        <Media />
      </div>
      <Footer />
    </div>
  );
};

export default MovieDetail;
