import React, { useState } from "react";
import styles from "./style.module.scss";
import { CircularProgressbar } from "react-circular-progressbar";
import { AiOutlinePercentage } from "react-icons/ai";
import dateFormat from "dateformat";
import { Modal } from "../common/Modal";
import { Link, NavLink } from "react-router-dom";
import { POSTER_URL } from "constants/common";

const MovieCard = React.forwardRef(
  ({ id, title, poster_path, release_date, vote_average }, ref) => {
    const [isModalOpen, setModalOpen] = useState(false);
    const handleClick = () => {
      setModalOpen(true);
    };
    const percentage = vote_average * 10;
    return (
      <div className={styles.card} ref={ref}>
        <div className={styles.card__image}>
          <div className={styles.card__wrapper}>
            <Link to={`/movie/${id}`} className={styles[`card__image-link`]}>
              <img
                alt="Movie x"
                className={styles.poster}
                loading="lazy"
                src={`${POSTER_URL}${poster_path}`}
              />
            </Link>
          </div>
          {isModalOpen ? (
            <Modal setModalOpen={setModalOpen} />
          ) : (
            <div className={styles.option} onClick={handleClick}>
              <div className={styles[`option__circle-more`]} />
            </div>
          )}
        </div>
        <div className={styles.content}>
          <div className={styles[`content__consensus`]}>
            <div className={styles[`content__outer-ring`]}>
              <div className={styles[`content__user-score`]}>
                <CircularProgressbar
                  value={percentage}
                  text={`${percentage}`}
                  styles={{
                    // Customize the path
                    path: {
                      stroke:
                        percentage >= 70
                          ? `rgba(33, 208, 122, ${percentage / 100})`
                          : `rgba(210,213,49, ${percentage / 100})`,

                      strokeLinecap: "round",
                    },
                    // Customize the circle behind the path
                    trail: {
                      stroke: percentage >= 70 ? "#204529" : "#423d0f",
                      strokeLinecap: "round",
                    },
                    text: {
                      fill: "#f2f3f4",
                      fontSize: "2.3rem",
                      fontWeight: "bold",
                    },
                  }}
                />
                <div className={styles.percent}>
                  <AiOutlinePercentage size={6} style={{ fill: "white" }} />
                </div>
              </div>
            </div>
          </div>
          <h2>
            <NavLink to={`/movie/${id}`}>{title}</NavLink>
          </h2>
          <p>{dateFormat(release_date, "mediumDate")}</p>
        </div>
      </div>
    );
  }
);

export default MovieCard;
